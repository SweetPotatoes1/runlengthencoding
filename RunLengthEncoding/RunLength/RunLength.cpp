// RunLength.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

bool isDigit(char a)
{
	return (48 <= int(a) && int(a) <= 57);
}
bool isSpecialChar(char a)
{
	return (a == '-') || (a == '(') || (a == ')');
}
int CharToInt(char a)
{
	return (int)a - 48;
}
//writes characters from vector of Distinct Conescutive digits (or special characters)
void writeDistinctNumbers(ofstream &writeFile, vector<char> &numbersToWrite, int& numberscount, int &outChars)
{
	if (numberscount >= 1 && numberscount <= 9)
	{
		writeFile << -numberscount;
		outChars += 2;
		for (int i = 0; i < numbersToWrite.size(); i++)
		{
			writeFile << numbersToWrite[i];
			outChars++;
		}
	}
	else if (numberscount >= 10)
	{
		writeFile << '(' << '-' << numberscount << ')';
		int digits = 0;
		int number = numberscount;
		while (number > 0)
		{
			number /= 10;
			digits++;
		}
		outChars += 3 + digits;
		for (int i = 0; i < numberscount; i++)
		{
			writeFile << numbersToWrite[i];
			outChars++;
		}
	}
	numbersToWrite.clear();
	numberscount = 1;
}
//writes characters(that are not digits) or adds them to the array(if they are digits)
void writeCharacters(ofstream &writeFile, int count, char &first, char &next, int &numberscount, vector<char> &numbersToWrite, int &outChars, bool &bNeedToWrite)
{
	if (count >= 2 && count <= 9)
	{
		writeFile << count << first;
		outChars += 2;
		bNeedToWrite = true;
	}
	if (count >= 10)

	{
		writeFile << "(" << count << ")"<<first;
		int digits = 0;
		while (count > 0)
		{
			count /= 10;
			digits++;
		}
		outChars += 3 + digits;
	}
	if (count == 1)
	{
		if (!isDigit(next) && !isSpecialChar(next))
		{
			if (numberscount >= 2)
			{
					writeDistinctNumbers(writeFile, numbersToWrite, numberscount, outChars);
					bNeedToWrite = true;
			}
			else if (numberscount == 1)
			{
				if ((isDigit(first) || isSpecialChar(first))) 
				{
					writeFile << "-1" << first;
					outChars += 3;
				}
				else
				{
					writeFile << first;
					outChars++;
				}
				bNeedToWrite = true;
			}
		}
		else if ((isDigit(first) || isSpecialChar(first)))
		{
			if (numberscount == 1)
			{
				numbersToWrite.push_back(first);
				numbersToWrite.push_back(next);
			}
			else
			{
				numbersToWrite.push_back(next);
			}
			numberscount++;
		}
		else
		{
			writeFile << first;
			outChars++;
			bNeedToWrite = true;
		}
	}
}
int main()
{	
	char answer1;
	char answer[30];
	char first, next;
	ifstream readFile;
	ofstream writeFile;
	float compressionrate;
	int inChars = 0, outChars = 0;
	bool bNeedToWrite = false;      //checks if there are unwritten characters when reaching the end of the file 
	bool shouldRepeatLoop = true;
	bool bDistinctNumbers = false;
	int count = 1;					//counts similar characters 
	int numberscount = 1;			//counts consecutive characters if they are digits(or special characters)
	vector<char> numbersToWrite;	//stores values of consecutive distinct digits

	do
	{
	    cout << "Would you like to compress a file or decompress a file encoded through this algorithm? (C-Compress/D-Decompress)" << endl;
		cin >> answer1;
		if (answer1 == 'C' || answer1 == 'D')
			shouldRepeatLoop = false;
	}
	while (shouldRepeatLoop);
	cout << "Enter the name of your file:" << endl;
	cin >> answer;
	readFile.open(answer);
	writeFile.open("Output.txt");
	if (!readFile.is_open())
	{
		exit(EXIT_FAILURE); 
	}
	if (answer1 == 'C')
	{
		if (!readFile.eof())
		{
			readFile.get(first);
			inChars++;
		}
		while (readFile.get(next))
		{
			inChars++;
			bNeedToWrite = false;
			if (first == next)
			{
				count++;
				bNeedToWrite = true;

				//This sequence checks if we should write any digits in our file when encountering a new sequence of similar digits(Example: 12344) 
				//Characters used when compressing the file '(' , ')', '-' are treated as digits
				if ((isDigit(first) || isSpecialChar(first)) && numbersToWrite.size() >= 1 && count == 2)
				{
					numbersToWrite.pop_back(); //at this stage, first is in the array of distinct consecutive numbers, and because next = first, we remove it from the array
					numberscount--;
					writeDistinctNumbers(writeFile, numbersToWrite, numberscount, outChars);
				}
			}
			else
			{
				writeCharacters(writeFile, count, first, next, numberscount, numbersToWrite, outChars, bNeedToWrite);
				count = 1;
				first = next;
			}
		}
		if (bNeedToWrite && count >= 2)
		{
			writeCharacters(writeFile, count, first, next, numberscount, numbersToWrite, outChars, bNeedToWrite);
		}
		else if (bNeedToWrite && count == 1)
		{
			if (!isDigit(first) && !isSpecialChar(first))
			{
				writeFile << first;
				outChars++;
			}
			else
			{
				writeFile << "-1" << first;
				outChars += 3;
			}
		}
		if (!numbersToWrite.empty())
		{
			writeDistinctNumbers(writeFile, numbersToWrite, numberscount, outChars);
		}
		compressionrate = (float)inChars / outChars;
		cout << "compression rate is " << compressionrate << endl;  
		cout << inChars << " initial characters" << endl;
		cout << outChars <<" characters when compressed" << endl;
	}
	else if (answer1 == 'D')
	{
		while (readFile.get(first))
		{
			if (isDigit(first))
			{
				readFile.get(next);
				for (int i = 0; i < CharToInt(first); i++)
				{
					writeFile << next;
				}
			}
			else if (first == '-')
			{
				readFile.get(next);
				for (int i = 0; i < CharToInt(next); i++)
				{
					readFile.get(first);
					writeFile << first;
				}
			}
			else if (first == '(')
			{
				int number = 0;
				readFile.get(next);
				if (next == '-')
				{
					bDistinctNumbers = true;
					readFile.get(next);
				}
				while (next != ')')
				{
					number = number * 10 + CharToInt(next);
					readFile.get(next);
				}
				//readFile.get(next);
				if (bDistinctNumbers)
				{
					for (int i = 0; i < number; i++)
					{
						readFile.get(next);
						writeFile << next;
					}
					bDistinctNumbers = false;
				}
				else
				{
					readFile.get(next);
					for (int i = 0; i < number; i++)
					{
						writeFile << next;
					}
				}	
			}
			else
			{
				writeFile << first;
			}
		}
	}
	readFile.close();
	writeFile.close();
	getchar();
	return  0;
}


