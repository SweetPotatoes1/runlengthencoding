Compressing:
Input:bbbbsssddd bds wwwweeee lk kkjsdhfnnnn hhhhiiiiiqwe wieiekkkkri kk3p sjddjdddiop ioooiieeerruuuuuu
Output:4b3s3d bds 4w4e lk 2kjsdhf4n 4h5iqwe wieie4kri 2k-13p sj2dj3diop i3o2i3e2r6u

Input:4444444423455623456.4555555878888888888886555556764566533333295292834j
Output:84-323425-6623456.-1465-287(12)8-1655-56764526-1553-9295292834j

Decompressing:
Input:4b3s3d bds 4w4e lk 2kjsdhf4n 4h5iqwe wieie4kri 2k-13p sj2dj3diop i3o2i3e2r6u
Output:bbbbsssddd bds wwwweeee lk kkjsdhfnnnn hhhhiiiiiqwe wieiekkkkri kk3p sjddjdddiop ioooiieeerruuuuuu

Input:84-323425-6623456.-1465-287(12)8-1655-56764526-1553-9295292834j
Output:4444444423455623456.4555555878888888888886555556764566533333295292834j

Steps to build/run the program:
-Extract the .zip file
-Run the .exe file or open the solution (.sln) visual studio file and build from inside the editor
- Enter either 'C' or 'D' when the text "Would you like to compress a file or decompress a file encoded through this algorithm? (C-Compress/D-Decompress)" appears on screen
- Enter the name of the file you want to compress/decompress followed by a ".txt"
- An "output.txt" will be generated in the same directory where the application was ran from.
Notes:
-if selecting 'D' for a Decompress, make sure that the file you enter is a file previously compressed by this algorithm. Any other random file will generate unwanted behaviour, which is to be expected 
-if you want to try other text files besides the ones provided in the .zip make sure that they are in the same directory as either the .exe(\PlaygroundGames_Engineering Test\RunLengthEncoding\Debug) or the .cpp(\PlaygroundGames_Engineering Test\RunLengthEncoding\RunLength) file

Steps taken to verify the program is correct:
-the program has been tested on many different types of input, including numbers and special characters
-multiple scenarios were taken into consideration

My solution:
The algorithm is generating the output file as it is iterating through the input file, in order to maximize efficiency, the complexity of this algorithm being O(n).
It is reading characters from the input file one by one and comparing it to the previous one. If two consecutive characters are equal, an int variable 'count' is incremented.
If they are not equal and count is greater than one, it means there was a sequence of 'count' similar characters exclusively until the last character read. And thus, the algorithm prints 'count' and the previous character read. 
Example: input is aaab, output is 3ab
If count is greater than 10 however, we use brackets to separate the terms.
Example: input is aaaaaaaaaaaaaaaab, output is (16)ab
If count is one however, it means there are no sequences of similar characters and so, we simply print the characters individually
Example: input is asdadxa, output is asdadxa.

In order for the algorithm to also work when the input contains numbers, a few modifications need to be made:
If we encounter consecutive characters that are similar numbers, the algorithm behaves exactly the same.
Example: input is 1111x, output is 41x
Example2: input is 11111111111x, output is (11)1x

However, when encountering a sequence of numbers that are different, the algorithm will count the length of that sequence
This is done by storing the characters in a vector when both the current character and the previous character read from the input file are digits. 
Upon encountering a character that is not a digit or a digit identic to the previous one(in this case, the last element from the vector gets removed), the function 'writeDistinctNumbers' is being called, which writes -length, followed by the array in the output file
Example: input is 125692, output is -6125692
This is useful because when performing a decompress, when encountering a '-' sign, the following number will represent the number of characters that should next be written and not interpreted as coefficients.
When the sequence is greater than 10, the same rules as before apply. Brackets will be used.
Example: input is 123456789231, output is (-12)123456789231

Special Cases: 
***Encountering '-', '(' and ')' in the input file.
It is important to take into account the possibility of having these characters in the input file.
Imagine the next scenario:
Input is: asxx(11)82
Output would be: as2x(21)-282
If we were to decompress this file by the rules we implemented so far we would get: asxx---------------------82. 
The decoder would interpret what is inside the brackets as a coefficient for the next character, thus writing the minus sign 21 times.
As a solution for this, both the minus sign and the brackets are treated in the same way as digits by the algorithm.
If encountered once, they will be outputed with a -1 before them.
Example: input is: (sss), Output is: -1(3s-1)
They will be added to the vector of numbers.
Example: input is 231(-233345)))--21, output is: -6231(-233-2453)2--221
To explain: the first 6 characters are all either numbers or special characters that are distinct in relation to the previous one. 
thus -6, and then the sequence itself 231(-2
Then, we have 333, which gets written as 33    (three threes)
Then 45, written as -245
Then ))), written as 3)
Then --, written as 2-
Then 21, written as -221 

***Not writing anything
Consider the following case:
Input is: 222222222222222222
Output will be nothing. 
The algorithm only increases the count variable, but it does not find a different character until the end of file and thus, does not know when to write (18)2

To solve this, the boolean bNeedsToWrite keeps track of wether there are unwritten characters. It gets set to false at the start of every read from the file, so when reaching the end of file, it will exit the loop as true if there are unwritten characters.
After the loop, we check for the boolean and if it is true we write 'count' and the last read number.
We also check to see if the array of distinc numbers is empty and if it is not we call the function 'writeDistinctNumbers'

Now for the decompressor:
We simply reverse engineer the initial algorithm.
When reading the input file(an already encoded file), we are looking for 3 cases.
Either a number, a minus sign, or an open bracket.
If we encounter a number, we print the following character that number of times.
If we encounter a - sign, we read the next character which will be a number, let's call it x, and then we print out the following x characters.
If we encounter an open bracket, we read the next character to see if it's a minus sign or not and store that result in a boolean. Then, we keep reading characters and construct our 'number' until we read the closing bracket.
We do this because we don't know how many digits the number in the brackets will have. it could be (12) or (221346), though highly unlikely.
If that first character after the paranthesis was a - sign, after the closing paranthesis we simply print the next 'number' characters. If not, we print the next character a 'number' amount of times.
If none of these conditions are met, we simply write the character because that means nothing was changed from the original file. 
Example: input is: ksx, output is: ksx. k is not a number, a - sign or an open bracket so we write it. The same for s and x.

When decompressing, we don't need to do any further checks because we are expecting a .txt file that was encoded using this algorithm and so, we simply follow the rules.
For example, we are sure that an open paranthesis'(' will only appear within a sequence of numbers{example: -6241(21   ->  251(21 }, or it will be followed by at least one digit before the closing bracket{example: (11)a   ->  aaaaaaaaaaa}   


Positives:
-Linear complexity
-Works on input .txt files containing any ASCII character

Negatives:
-There are cases when compressing, where the output file is larger than the input one (0 < compression rate < 1), when encountering short sequences of distinc numbers/special characters
Example: Input is: 


